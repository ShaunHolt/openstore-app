# OpenStore App Changelog

## v3.2.0

- Added sorting apps
- Added filtering apps by type

## v3.1.6

- Updated translations, thank you translators!

## v3.1.5

- Updated translations, thank you translators!

## v3.1.4

- Updated translations, thank you translators!

## v3.1.3

- New splash screen
